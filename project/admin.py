from django.contrib import admin
from .models import Functions
from .models import Koshi
from .models import Integrations


admin.site.register(Functions)
admin.site.register(Koshi)
admin.site.register(Integrations)