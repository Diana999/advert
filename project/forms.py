from django import forms
#from .models import SingUp
from .models import Functions
from .models import Koshi
from .models import Integrations

class FunctionsForm(forms.ModelForm):
    class Meta:
        model = Functions
        fields = '__all__'


class KoshiForm(forms.ModelForm):
    class Meta:
        model = Koshi
        fields = '__all__'


class IntegrationsForm(forms.ModelForm):
    class Meta:
        model = Integrations
        fields = '__all__'