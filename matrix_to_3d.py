
from __future__ import division
import sys

def matrix_to_3d_for_3(A):
	n = 3
	first = [ A[0][i] for i in range(n) ]
	second = [ A[1][i] for i in range(n)]
	third =  [  A[2][i] for i in range(n)]
	for i in range(3):
		first[i] = first[i]*A[1][2] -second[i]*A[0][2]
		third[i] = third[i]*A[1][0] - second[i]*A[2][0]
	return first, second, third
a, b, c = matrix_to_3d_for_3([[1,2,3], [2.0001,3.999,6], [15,3,6]])
A = [a, b, c]
print (A)
print A[0][1]
def progon(A, vec):
	n = len(A)
	a_coeffs = [-A[0][1]/A[0][0]]
	b_coeffs = [vec[0]/A[0][0]]
	for i in range(1, 3):
		a_coeffs.append(-A[i][i+1]/(A[i][i]+A[i][i-1]*a_coeffs[i-1]))
		b_coeffs.append((-A[i][i-1]*b_coeffs[i-1] + vec[i])/(A[i][i]+A[i][i-1]*a_coeffs[i-1]))
	b_coeffs.append((-A[3][3-1]*b_coeffs[3-1] + vec[3])/(A[3][3]+A[3][3-1]*a_coeffs[3-1]))
	answer = [0]*(n)
	answer[n-1] = b_coeffs[len(b_coeffs)-1]
	for i in range(n-1):
		answer[n-i-2] = a_coeffs[len(a_coeffs)-1 -i]*answer[n-1-i]+b_coeffs[len(b_coeffs)-2-i]
	
	return answer
#print progon(A, [1,2,3])
#print progon([[2,1,0,0], [1,10,-5,0], [0, 1, -5, 2], [0,0,1,4]], [-5, -18, -40, -27])

def norm(A): #Вычисление нормы матрицы
	maxi = -100000
	for i in A:
		curr = 0
		for j in i:
			curr += abs(j)
		if curr > maxi:
			maxi = curr
	return maxi