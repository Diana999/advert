# -*- coding: utf-8 -*-
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.function, name='function'),
    url(r'^params', views.function_new, name='function_new'),
    url(r'^integral', views.integral_save, name='integral_save'),
    url(r'^koshi', views.koshi, name='koshi'),

]