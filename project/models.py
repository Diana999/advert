from django.db import models
from django.utils import timezone


class Functions(models.Model):
    a = models.IntegerField("Введите первый параметр p(t)")
    b=models.IntegerField("Введите второй параметр p(t)")
    c=models.IntegerField("Введите второй параметр z(t)")
    d=models.IntegerField("Введите второй параметр z(t)")
    f=models.IntegerField("Введите второй параметр S(t)")
    g=models.IntegerField("Введите второй параметр S(t)")
    h = models.IntegerField("При желании можно ввести шаг табуляции", blank=True, default=0)
    a_p = models.IntegerField("Введите верхний предел интегрирования функции p(t).", default=0)
    b_p = models.IntegerField("Введите нижний предел интегрирования функции p(t).", default=1)


    def publish(self):

        self.save()


class Koshi(models.Model):
    x0 = models.IntegerField("Введите параметр x0 задачи Коши")
    y0 = models.IntegerField("Введите параметр y0 задачи Коши")
    b_c = models.IntegerField("Введите параметр b задачи Коши")
    T = models.IntegerField("Введите параметр сетки задачи Коши")

    def publish(self):
        self.save()

class Integrations(models.Model):

    nn = models.IntegerField("Введите n - количество шагов, по умолчанию = 10", default=10)

    def publish(self):
        self.save()



